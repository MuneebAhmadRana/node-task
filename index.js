const os = require("os");
const fs = require("fs");
const http = require("http");
const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
 


rl.question(
  "Choose an option \n 1. Read package.json \n 2. Display OS info \n 3. Start HTTP server\n Type in a number: ",
  (input) => {
    switch (input) {
      case "1":
        readFile();
        break;
      case "2":
        checkOSInfo();
        break;
      case "3":
        createServer();
        break;

      default:
        console.log("Invalid Option: Please pick a number between 1 2 and 3");
        break;
    }
    rl.close();
  }
);

readFile = () => {
  fs.readFile(__dirname + "/package.json", "utf-8", (err, content) => {
    console.log(content);
  });
}
checkOSInfo = () => {
  const totalMemory =
          (os.totalmem() / 1024 / 1024 / 1024).toFixed(2) + "GB";
        const freeMemory =
          (os.freemem() / 1024 / 1024 / 1024).toFixed(2) + "GB";
        const cpuCores = os.cpus().length;
        const archs = os.arch();
        const platform = os.platform();
        const release = os.release();
        const user = os.userInfo().username;
        console.log(
          "SYSMEM MEMORY: ",
          totalMemory,
          "\n Free Memory: ",
          freeMemory,
          "\n CPU CORES: ",
          cpuCores,
          "\n ARCH: ",
          archs,
          "\n PLATFORM: ",
          platform,
          " \n RELEASE: ",
          release,
          "\n USER: ",
          user
        );
}
createServer = () => {
  const server = http.createServer((req, res) => {
    if (req.url === "/") {
      res.write("hello world!");
      res.end();
      return;
    }
    console.log("Page does not exist");
  });
  server.listen(3000);
  console.log("Server started at port 3000");
}